<?php

namespace App\Http\Controllers;

use App\Models\Book;
use App\Models\Student;
use App\Models\Category;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $students = Student::orderBy('id', 'desc')->get();
        $books = Book::orderBy('title', 'asc')->get();
        $categories = Category::all();

        return view('home', ['students' => $students, 'books' => $books,'categories' => $categories]);
    }
}
