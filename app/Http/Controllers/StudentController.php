<?php

namespace App\Http\Controllers;

use App\Models\Book;
use App\Models\Student;
use Illuminate\Http\Request;

class StudentController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        // dd($request->all());
        $student = Student::create([
            'name' => $request->input('name'),
            'first_name' => $request->input('first_name'),
            'gender' => $request->input('gender'),
            'level' => $request->input('level'),
            'address' => $request->input('address'),
            'contact' => $request->input('contact'),
            'establishment' => $request->input('establishment'),
            'courses' => $request->input('courses'),
            'book' => $request->input('book')
        ]);

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function show(Student $student)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function edit(Student $student)
    {
        $books = Book::orderBy('title', 'asc')->get();
        // dd($books);
        return view('student.edit', ['student' => $student, 'books' => $books]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Student $student)
    {
        $student->update([
            'name' => $request->input('name'),
            'first_name' => $request->input('first_name'),
            'gender' => $request->input('gender'),
            'level' => $request->input('level'),
            'address' => $request->input('address'),
            'contact' => $request->input('contact'),
            'establishment' => $request->input('establishment'),
            'courses' => $request->input('courses'),
            'book' => $request->input('book')
        ]);

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function destroy(Student $student)
    {
        //
    }
}
