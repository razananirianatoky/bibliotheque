<div>
    <div>
        <form class="d-flex align-content-center mb-2 form-control border-0 rounded-0 border-bottom" id="search-form">
            <img src="images/loop.svg" width="20">
            <input class="me-2 border-0 rounded-0 w-100 pb-0 mb-0 fs-6 px-1" type="search" id="search"
                placeholder=" Rechercher" wire:model.debounce.500ms="search">
        </form>
    </div>  
    <table class="table table-dark table-striped">
        <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">
                    <div class="row">
                        <div class="col-6">Nom</div>
                        <div class="col text-end"><img src="images/sort.svg" width="15" id="arrow-sort"></div>
                    </div>
                </th>
                <th scope="col">
                    <div class="row">
                        <div class="col-6">Prénom</div>
                        <div class="col text-end"><img src="images/sort.svg" width="15" id="arrow-sort"></div>
                    </div>
                </th>
                <th scope="col">Livre</th>
                <th scope="col">
                    <div class="row">
                        <div class="col-6">Etablissement</div>
                        <div class="col text-end"><img src="images/sort.svg" width="15" id="arrow-sort"></div>
                    </div>
                </th>
                <th scope="col">
                    <div class="row">
                        <div class="col-6">Niveau</div>
                        <div class="col text-end"><img src="images/sort.svg" width="15" id="arrow-sort"></div>
                    </div>
                </th>
                <th scope="col">Contact</th>
                <th scope="col">
                    <div class="row">
                        <div class="col-6">Début</div>
                        <div class="col text-end"><img src="images/sort.svg" width="15" id="arrow-sort"></div>
                    </div>
                </th>
                <th scope="col">
                    <div class="row">
                        <div class="col-6">Fin</div>
                        <div class="col text-end"><img src="images/sort.svg" width="15" id="arrow-sort"></div>
                    </div>
                </th>
                <th scope="col">Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($students as $student)
            <tr>
                <th scope="row">{{ $student->id }}</th>
                <td>{{ $student->name }}</td>
                <td>{{ $student->first_name }}</td>
                <td>{{ $student->book }}</td>
                <td>{{ $student->establishment }}</td>
                <td>{{ $student->level }}</td>
                <td>{{ $student->contact }}</td>
                <td>{{ $student->created_at->format('d/m h:i') }}</td>
                <td>{{ $student->updated_at->format('d/m h:i') }}</td>
                <td>
                    <div class="btn-group" role="group" aria-label="Basic mixed styles example">
                        <button type="button" class="btn btn-outline-secondary">Voir</button>
                        <button type="button" class="btn btn-outline-success edit-student" data-bs-toggle="modal"
                        data-bs-target="#editStudent" data-url="{{ route('student.edit', $student->id) }}">Modifier</button>
                    </div>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
