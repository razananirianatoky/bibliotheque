@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row align-items-end mb-2">
        <div class="col-sm">
            <h5 class="mb-0">Liste des élèves</h5>
        </div>
        <div class="col-sm text-end">
            <button type="button" class="btn btn-outline-secondary rounded-0" data-bs-toggle="modal"
                data-bs-target="#addStudent">Ajouter</button>
        </div>
    </div>
    <div>
        <form class="d-flex align-content-center mb-2 form-control border-0 rounded-0 border-bottom" id="search-form">
            <img src="images/loop.svg" width="20">
            <input class="me-2 border-0 rounded-0 w-100 pb-0 mb-0 fs-6 px-1" type="search" id="search"
                placeholder=" Rechercher" wire:model.debounce.500ms="search">
        </form>
    </div>
    <table class="table table-dark table-striped">
        <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">
                    <div class="row">
                        <div class="col-6">Nom</div>
                        <div class="col text-end"><img src="images/sort.svg" width="15" id="arrow-sort"></div>
                    </div>
                </th>
                <th scope="col">
                    <div class="row">
                        <div class="col-6">Prénom</div>
                        <div class="col text-end"><img src="images/sort.svg" width="15" id="arrow-sort"></div>
                    </div>
                </th>
                <th scope="col">Livre</th>
                <th scope="col">
                    <div class="row">
                        <div class="col-6">Etablissement</div>
                        <div class="col text-end"><img src="images/sort.svg" width="15" id="arrow-sort"></div>
                    </div>
                </th>
                <th scope="col">
                    <div class="row">
                        <div class="col-6">Niveau</div>
                        <div class="col text-end"><img src="images/sort.svg" width="15" id="arrow-sort"></div>
                    </div>
                </th>
                <th scope="col">Contact</th>
                <th scope="col">
                    <div class="row">
                        <div class="col-6">Début</div>
                        <div class="col text-end"><img src="images/sort.svg" width="15" id="arrow-sort"></div>
                    </div>
                </th>
                <th scope="col">
                    <div class="row">
                        <div class="col-6">Fin</div>
                        <div class="col text-end"><img src="images/sort.svg" width="15" id="arrow-sort"></div>
                    </div>
                </th>
                <th scope="col">Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($students as $student)
            <tr>
                <th scope="row">{{ $student->id }}</th>
                <td>{{ $student->name }}</td>
                <td>{{ $student->first_name }}</td>
                <td>{{ $student->book }}</td>
                <td>{{ $student->establishment }}</td>
                <td>{{ $student->level }}</td>
                <td>{{ $student->contact }}</td>
                <td>{{ $student->created_at->format('d/m h:i') }}</td>
                <td>{{ $student->updated_at->format('d/m h:i') }}</td>
                <td>
                    <div class="btn-group" role="group" aria-label="Basic mixed styles example">
                        <button type="button" class="btn btn-outline-secondary">Voir</button>
                        <button type="button" class="btn btn-outline-success edit-student" data-bs-toggle="modal"
                        data-bs-target="#editStudent" data-url="{{ route('student.edit', $student->id) }}">Modifier</button>
                    </div>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>

<!--Modal For Creating Student -->
<div class="modal fade" id="addStudent" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1"
    aria-labelledby="addStudentLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content rounded-0">
            <div class="modal-header">
                <h5 class="modal-title" id="addStudentLabel">Ajouter</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form action="{{ route('student.store') }}" class="row g-3" method="POST">
                    @csrf
                    <div class="col-md-6">
                        <label for="name" class="form-label">Nom</label>
                        <input type="text" class="form-control rounded-0 border-0 border-bottom bg-input" id="name"
                            name="name" placeholder="Votre nom">
                    </div>
                    <div class="col-md-6">
                        <label for="first_name" class="form-label">Prénom</label>
                        <input type="text" class="form-control rounded-0 border-0 border-bottom bg-input"
                            id="first_name" name="first_name" placeholder="Votre prénom">
                    </div>
                    <div class="col-12">
                        <label for="establishment" class="form-label">Etablissement</label>
                        <input type="text" class="form-control rounded-0 border-0 border-bottom bg-input"
                            id="establishment" name="establishment" placeholder="Votre etablissement">
                    </div>
                    <div class="col-12">
                        <label for="book" class="form-label">Livre</label>
                        <select class="form-select rounded-0 border-0 border-bottom bg-input"
                            aria-label="Default select example" id="book" name="book">
                            <option selected>Choisir un livre</option>
                            @foreach ($books as $book)
                            <option value="{{ $book->title }}">{{ $book->title }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-6">
                        <label for="level" class="form-label">Niveau</label>
                        <input type="text" class="form-control rounded-0 border-0 border-bottom bg-input" id="level"
                            name="level" placeholder="Niveau d'etude">
                    </div>
                    <div class="col-md-6">
                        <label for="courses" class="form-label">Filière</label>
                        <input type="text" class="form-control rounded-0 border-0 border-bottom bg-input" id="courses"
                            name="courses" placeholder="ex: science">
                    </div>
                    <div class="col-12">
                        <label for="contact" class="form-label">Contact</label>
                        <input type="text" class="form-control rounded-0 border-0 border-bottom bg-input" id="contact"
                            name="contact" placeholder="03x xx xxx xx">
                    </div>
                    <div class="col-12">
                        <label for="address" class="form-label">Address</label>
                        <input type="text" class="form-control rounded-0 border-0 border-bottom bg-input" id="address"
                            placeholder="1234 Mahamasina" name="address">
                    </div>
                    <div class="form-check col-md-6 ms-2">
                        <input class="form-check-input" type="radio" name="gender" id="homme" value="Homme">
                        <label class="form-check-label" for="homme">
                            Homme
                        </label>
                    </div>
                    <div class="form-check col-md-6 mt-0 ms-2">
                        <input class="form-check-input" type="radio" name="gender" id="femme" value="Femme">
                        <label class="form-check-label" for="femme">
                            Femme
                        </label>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-secondary rounded-0">Enregistrer</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!--Modal For Creating Student -->
<div class="modal fade" id="editStudent" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1"
    aria-labelledby="editStudentLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content rounded-0">
            <div class="modal-header">
                <h5 class="modal-title" id="editStudentLabel">Modifier</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="modal-form">

                </div>
                <div class="d-flex justify-content-center spinner-modal">
                    <div class="spinner-border" role="status">
                        <span class="visually-hidden">Loading...</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
    <script src="{{ asset('js/student/edit.js') }}"></script>
@endsection