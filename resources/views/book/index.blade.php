@extends('layouts.app')

@section('content')
<div class="container-fluid">
  <div class="row">
    <div class="col-lg-3">
      <div class="d-grid gap-2 my-4">
        <button type="button" class="btn btn-outline-secondary rounded-0" data-bs-toggle="modal"
          data-bs-target="#addBook">Ajouter un nouveau livre</button>
      </div>
      <div class="card" style="width: 18rem;">
        <div class="card-header text-center bg-dark text-white">
          Filtrer par catégorie
        </div>
        <ul class="list-group list-group-flush">
          @foreach ($categories as $category)
          <li class="list-group-item list-group-item-action">{{ $category->name }}</li>
          @endforeach
        </ul>
      </div>
    </div>
    <div class="col-lg-9">
      <h5 class="">Liste de tous les livres</h5>
      <div>
        <form class="d-flex align-content-center mb-2 form-control border-0 rounded-0 border-bottom" id="search-form">
          <img src="images/loop.svg" width="20">
          <input class="me-2 border-0 rounded-0 w-100 pb-0 mb-0 fs-6 px-1" type="search" id="search"
            placeholder=" Rechercher un livre" wire:model.debounce.500ms="search">
        </form>
      </div>
      <table class="table table-dark table-striped">
        <thead>
          <tr>
            <th scope="col">
              <div class="row">
                <div class="col-6">Titre</div>
                <div class="col text-end"><img src="images/sort.svg" width="15" id="arrow-sort"></div>
              </div>
            </th>
            <th scope="col">
              <div class="row">
                <div class="col-6">Auteur</div>
                <div class="col text-end"><img src="images/sort.svg" width="15" id="arrow-sort"></div>
              </div>
            </th>
            <th scope="col">Action</th>
          </tr>
        </thead>
        <tbody>

          @foreach ($books as $book)
          <tr>
            <td>{{ $book->title }}</td>
            <td>{{ $book->author }}</td>
            <td>
              <div class="btn-group" role="group" aria-label="Basic mixed styles example">
                <button type="button" class="btn btn-outline-success edit-book" data-bs-toggle="modal"
                  data-bs-target="#editBook" data-url="{{ route('book.edit', $book->id) }}">Modifier</button>
                <button type="button" class="btn btn-outline-danger">Supprimer</button>
              </div>
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </div>
</div>

<!-- Modal for adding book -->
<div class="modal fade" id="addBook" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1"
  aria-labelledby="addBookLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content rounded-0">
      <div class="modal-header">
        <h5 class="modal-title" id="addBookLabel">Ajouter un nouveau livre</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <form action="{{ route('book.store') }}" method="post">
          @csrf
          <div class="col-12">
            <label for="title" class="form-label">Titre</label>
            <input type="text" class="form-control rounded-0 border-0 border-bottom bg-input" id="title" name="title"
              placeholder="Titre du livre">
          </div>
          <div class="col-12">
            <label for="author" class="form-label">Auteur</label>
            <input type="text" class="form-control rounded-0 border-0 border-bottom bg-input" id="author" name="author"
              placeholder="Nom de l'auteur">
          </div>
          <div class="col-12">
            <label for="category" class="form-label">Categorie</label>
            <select class="form-select rounded-0 border-0 border-bottom bg-input" aria-label="Default select example"
              id="category" name="category_id">
              <option selected value="">Choisir un categorie</option>
              @foreach ($categories as $category)
              <option value="{{ $category->id }}">{{ $category->name }}</option>
              @endforeach
            </select>
          </div>
          <div class="modal-footer mt-2">
            <button type="submit" class="btn btn-secondary rounded-0">Enregstrer</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<!-- Modal for editing book -->
<div class="modal fade" id="editBook" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1"
  aria-labelledby="editBookLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content rounded-0">
      <div class="modal-header">
        <h5 class="modal-title" id="editBookLabel">Ajouter un nouveau livre</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <div class="modal-form">

        </div>
        <div class="d-flex justify-content-center spinner-modal">
          <div class="spinner-border" role="status">
            <span class="visually-hidden">Loading...</span>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection

@section('js')
<script src="{{ asset('js/book/edit.js') }}"></script>
@endsection