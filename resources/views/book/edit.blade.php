<form action="{{ route('book.update', $book->id) }}" method="post">
  @csrf
  @method('put')
  <div class="col-12">
    <label for="title" class="form-label">Titre</label>
    <input type="text" class="form-control rounded-0 border-0 border-bottom bg-input" id="title" name="title"
      placeholder="Titre du livre" value="{{ $book->title }}">
  </div>
  <div class="col-12">
    <label for="author" class="form-label">Auteur</label>
    <input type="text" class="form-control rounded-0 border-0 border-bottom bg-input" id="author" name="author"
      placeholder="Nom de l'auteur" value="{{ $book->author }}">
  </div>
  <div class="col-12">
    <label for="category" class="form-label">Categorie</label>
    <select class="form-select rounded-0 border-0 border-bottom bg-input" aria-label="Default select example"
      id="category" name="category_id">
      <option selected value="">Choisir un categorie</option>
      @foreach ($categories as $category)
      @if ( $book->category_id == $category->id)
      <option value="{{ $category->id }}" selected>{{ $category->name }}</option>
      @endif
      <option value="{{ $category->id }}">{{ $category->name }}</option>
      @endforeach
    </select>
  </div>
  <div class="modal-footer mt-2">
    <button type="submit" class="btn btn-secondary rounded-0">Mettre a jour</button>
  </div>
</form>