<form action="{{ route('student.update', $student->id) }}" class="row g-3" method="POST">
  @csrf
  @method('put')
  <div class="col-md-6">
    <label for="name" class="form-label">Nom</label>
    <input type="text" class="form-control rounded-0 border-0 border-bottom bg-input" id="name" name="name"
      placeholder="Votre nom" value="{{ $student->name }}">
  </div>
  <div class="col-md-6">
    <label for="first_name" class="form-label">Prénom</label>
    <input type="text" class="form-control rounded-0 border-0 border-bottom bg-input" id="first_name" name="first_name"
      placeholder="Votre prénom" value="{{ $student->first_name }}">
  </div>
  <div class="col-12">
    <label for="establishment" class="form-label">Etablissement</label>
    <input type="text" class="form-control rounded-0 border-0 border-bottom bg-input" id="establishment"
      name="establishment" placeholder="Votre etablissement" value="{{ $student->establishment }}">
  </div>
  <div class="col-12">
    <label for="book" class="form-label">Livre</label>
    <select class="form-select rounded-0 border-0 border-bottom bg-input" aria-label="Default select example" id="book"
      name="book">
      <option value="">Choisir un livre</option>
      @foreach ($books as $book)
      @if ($book->title == $student->book)
      <option value="{{ $book->title }}" selected>{{ $book->title }}</option>
      @endif
      <option value="{{ $book->title }}">{{ $book->title }}</option>
      @endforeach
    </select>
  </div>
  <div class="col-md-6">
    <label for="level" class="form-label">Niveau</label>
    <input type="text" class="form-control rounded-0 border-0 border-bottom bg-input" id="level" name="level"
      placeholder="Niveau d'etude" value="{{ $student->level }}">
  </div>
  <div class="col-md-6">
    <label for="courses" class="form-label">Filière</label>
    <input type="text" class="form-control rounded-0 border-0 border-bottom bg-input" id="courses" name="courses"
      placeholder="ex: science" value="{{ $student->courses }}">
  </div>
  <div class="col-12">
    <label for="contact" class="form-label">Contact</label>
    <input type="text" class="form-control rounded-0 border-0 border-bottom bg-input" id="contact" name="contact"
      placeholder="03x xx xxx xx" value="{{ $student->contact }}">
  </div>
  <div class="col-12">
    <label for="address" class="form-label">Adresse</label>
    <input type="text" class="form-control rounded-0 border-0 border-bottom bg-input" id="address"
      placeholder="1234 Mahamasina" name="address" value="{{ $student->address }}">
  </div>
  <div class="form-check col-md-6 ms-2">
    @if ($student->gender == 'Homme')
    <input class="form-check-input" type="radio" name="gender" id="homme" value="Homme" checked>
    @else
    <input class="form-check-input" type="radio" name="gender" id="homme" value="Homme">
    @endif
    <label class="form-check-label" for="homme">
      Homme
    </label>
  </div>
  <div class="form-check col-md-6 mt-0 ms-2">
    @if ($student->gender == 'Femme')
    <input class="form-check-input" type="radio" name="gender" id="femme" value="Femme" checked>
    @else
    <input class="form-check-input" type="radio" name="gender" id="femme" value="Femme">
    @endif
    <label class="form-check-label" for="femme">
      Femme
    </label>
  </div>
  <div class="modal-footer">
    <button type="submit" class="btn btn-secondary rounded-0">Mettre a jour</button>
  </div>
</form>