$(function(){

  $('.edit-book').on('click', function()
  {
      $('#editBook .modal-spinner').removeClass('d-none');
      $('#editBook .modal-form').addClass('d-none');
      $('#editBook').modal();

      let url = $(this).attr('data-url');

      $.get(url, function(form)
      {
          $('#editBook .modal-form').html( form );
          $('#editBook .spinner-modal').addClass('d-none');
          $('#editBook .modal-form').removeClass('d-none');
      });
      
  });
})