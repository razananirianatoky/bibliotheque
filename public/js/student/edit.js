$(function(){

  $('.edit-student').on('click', function()
  {
      $('#editStudent .modal-spinner').removeClass('d-none');
      $('#editStudent .modal-form').addClass('d-none');
      $('#editStudent').modal();

      let url = $(this).attr('data-url');

      $.get(url, function(form)
      {
          $('#editStudent .modal-form').html( form );
          $('#editStudent .spinner-modal').addClass('d-none');
          $('#editStudent .modal-form').removeClass('d-none');
      });
      
  });
})